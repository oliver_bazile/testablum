## Application TestAlbum
L'application occupe d'une seul interface, il affiche la liste des albums. <br />
La base de donnée est sauvegarder une fois, s'il y a une connexion avec l'internet.<br /> 

## Installation
git clone https://gitlab.com/oliver_bazile/testablum.git

## Choix Architecture
J'ai utilisé le model d'une clean Architecture, afin de séparer les taches et de structurer l'application. <br />
-présentation : Interface UX( activity, fragment et widget) / dépendance de l'application, viewModel <br />
-domaine :  Fonction qui reflète l'action de l'application / model / repository <br />
-data: Serveur (Rétrofit)/base de donnée en locale (Room)/ repository (où je traite les échanges des données entre serveur et local) <br />
 
## Build variante/Flavors
dev: C'est la partie debug de l'application <br />
prod: La partie améliorer de l'application avec un design remodeler.<br />

## Choix des bibliothèques
Room: Pour la création de la base de donnée. <br />
View Binding: Pour éviter les appels redondant générer par la création des fichiers XML.<br />
Mockito : Pour effectuer des tests sur le viewmodel.<br />
Coil: Pour l'affichage des images. <br />
   


