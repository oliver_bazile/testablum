package com.example.testalbum.domain.model

data class Album(
    val id: Int?,
    val albumId: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)
