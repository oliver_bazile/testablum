package com.example.testalbum.domain.repository

import com.example.testalbum.domain.model.Album
import com.example.testalbum.util.Resource
import kotlinx.coroutines.flow.Flow

interface Repository {

    suspend fun getAlum(): Flow<Resource<List<Album>>>

}