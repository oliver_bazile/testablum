package com.example.testalbum.domain.buisness

import com.example.testalbum.domain.model.Album
import com.example.testalbum.domain.repository.Repository
import com.example.testalbum.util.Resource
import kotlinx.coroutines.flow.Flow

class GetAlbum(val repository: Repository) {

    suspend operator fun invoke() : Flow<Resource<List<Album>>> {
        return  repository.getAlum()
    }
}