package com.example.testalbum.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AlbumEntity(
    val albumId: Int,
    @PrimaryKey val id: Int? = null,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)