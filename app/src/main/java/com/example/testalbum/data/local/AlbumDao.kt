package com.example.testalbum.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(
        albumlist : List<AlbumEntity>
    )

    @Query("DELETE FROM albumentity")
    suspend fun clearAll()

    @Query("SELECT * FROM  albumentity")
    suspend fun getAllAlbum() : List<AlbumEntity>
}