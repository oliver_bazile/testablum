package com.example.testalbum.data.repository

import com.example.testalbum.data.api.AlbumApi
import com.example.testalbum.data.local.AlbumDatabase
import com.example.testalbum.data.mapper.toDomain
import com.example.testalbum.data.mapper.toDto
import com.example.testalbum.domain.model.Album
import com.example.testalbum.domain.repository.Repository
import com.example.testalbum.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject


class RepositoryImpl @Inject constructor(
    private val db: AlbumDatabase,
    val albumApi: AlbumApi
) : Repository {

    private val albumDao = db.dao

    override suspend fun getAlum(): Flow<Resource<List<Album>>> {
        return flow {
            emit(Resource.Loading(true))
            var localAlbum = albumDao.getAllAlbum().map { it.toDomain() }
            emit(Resource.Success(data = localAlbum))
            if (localAlbum.isEmpty()) {
                try {
                    val remoteAlbum = albumApi.getAlbum()
                    albumDao.clearAll()
                    albumDao.insert(remoteAlbum.map { it.toDto() })
                     emit(Resource.Loading(false))
                } catch (e: HttpException) {
                    e.printStackTrace()
                    emit(Resource.Error(message = "Couldn't load data"))
                    emit(Resource.Loading(false))
                } catch (e: IOException) {
                    emit(Resource.Error(message = "You need Connexion internet"))
                    emit(Resource.Loading(false))

                }
                localAlbum = albumDao.getAllAlbum().map { it.toDomain() }
            }
            emit(Resource.Success(localAlbum))
        }
    }
}