package com.example.testalbum.data.api

import com.example.testalbum.domain.model.Album
import retrofit2.http.GET

interface AlbumApi {

    @GET("img/shared/technical-test.json")
    suspend fun getAlbum() : List<Album>

    companion object{
        const val BASE = "https://static.leboncoin.fr/"
    }
}