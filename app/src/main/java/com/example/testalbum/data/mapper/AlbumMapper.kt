package com.example.testalbum.data.mapper

import com.example.testalbum.data.local.AlbumEntity
import com.example.testalbum.domain.model.Album


fun AlbumEntity.toDomain() = Album(
    albumId = albumId,
    thumbnailUrl = thumbnailUrl,
    title = title,
    url = url,
    id = id
)


fun Album.toDto() = AlbumEntity(
    albumId = albumId,
    thumbnailUrl = thumbnailUrl,
    title = title,
    url = url,
    id = id
)