package com.example.testalbum.presentation.fragment.album

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testalbum.domain.buisness.GetAlbum
import com.example.testalbum.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class AlbumViewModel
@Inject constructor(private val getAlbum: GetAlbum) : ViewModel() {

    private val _events = MutableStateFlow(AlbumEvent(isLoading = false))
    val event: StateFlow<AlbumEvent> = _events

    init {
        getAllAlbum()
    }

    fun getAllAlbum() {
        viewModelScope.launch {
            getAlbum.invoke().collect { result ->
                when (result) {
                    is Resource.Success -> {
                        result.data?.let { album ->
                            _events.value = AlbumEvent(albumList = album, isLoading = false)
                        }
                    }
                    is Resource.Error -> {
                        result.message?.let {
                            _events.value = AlbumEvent(error = it, isLoading = false)
                        }
                    }
                    is Resource.Loading -> {
                        _events.value = AlbumEvent(isLoading = true)
                    }
                }
            }
        }
    }
}
