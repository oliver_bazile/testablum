package com.example.testalbum.presentation.widget

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.res.ResourcesCompat
import com.example.testalbum.R

class TextViewPacifico @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : androidx.appcompat.widget.AppCompatTextView(context, attrs) {
    init {
        this.typeface = ResourcesCompat.getFont(context, R.font.pacifico_regular)
    }
}
