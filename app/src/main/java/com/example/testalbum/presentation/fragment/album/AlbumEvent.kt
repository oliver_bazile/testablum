package com.example.testalbum.presentation.fragment.album

import com.example.testalbum.domain.model.Album

data class AlbumEvent(
    val isLoading: Boolean = false,
    val albumList :List<Album> = emptyList(),
    val error : String = ""
)
