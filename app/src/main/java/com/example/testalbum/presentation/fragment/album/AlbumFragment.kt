package com.example.testalbum.presentation.fragment.album

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testalbum.R
import com.example.testalbum.databinding.FragmentAlbumBinding
import com.example.testalbum.presentation.fragment.AlbumAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AlbumFragment : Fragment() {

    lateinit var adapter: AlbumAdapter
    private val viewModel: AlbumViewModel by viewModels()
    private lateinit var binding: FragmentAlbumBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentAlbumBinding.bind(view)
        initRecyclerView()
        getMovie()

    }

    private fun initRecyclerView() {
        binding.albumRecyclerViewFragment.apply {
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun getMovie() {
        lifecycleScope.launchWhenStarted {
            viewModel.event.collect { response ->
                if (!response.isLoading) {
                    binding.progressBar.visibility = View.INVISIBLE
                    if (response.error.isNotBlank()) {
                        Toast.makeText(activity, response.error, Toast.LENGTH_LONG).show()
                    } else {
                        adapter = AlbumAdapter(response.albumList)
                        binding.albumRecyclerViewFragment.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        }

    }
}