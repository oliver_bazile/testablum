package com.example.testalbum.presentation.fragment

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.bumptech.glide.Glide
import com.example.testalbum.R
import com.example.testalbum.databinding.RecyclerViewBinding
import com.example.testalbum.domain.model.Album


class AlbumAdapter(val albumList: List<Album>): RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder>() {
    inner class AlbumViewHolder(private val binding: RecyclerViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(album: Album) {

            val urlImage = StringBuilder().apply {
                append(album.url)
                append(".png")
            }

            binding.imageViewAlbumFragment.load(urlImage.toString())

            val titleText = binding.textViewTitleAlbumFragment
            titleText.text = String.format(
                titleText.context.getString(R.string.title_album_recycler_view),
                album.title
            )
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AlbumAdapter.AlbumViewHolder {
        val binding = RecyclerViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AlbumViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AlbumAdapter.AlbumViewHolder, position: Int) {
        val currentAlbum = albumList[position]
        holder.bind(currentAlbum)
    }

    override fun getItemCount(): Int = albumList.size
}