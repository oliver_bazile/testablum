package com.example.testalbum.presentation.di

import android.app.Application
import androidx.room.Room
import com.example.testalbum.data.api.AlbumApi
import com.example.testalbum.data.local.AlbumDatabase
import com.example.testalbum.data.repository.RepositoryImpl
import com.example.testalbum.domain.buisness.GetAlbum
import com.example.testalbum.domain.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AlbumModule {

    @Singleton
    @Provides
    fun providesAlbumApi() : AlbumApi {
        return Retrofit
            .Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(AlbumApi.BASE)
            .build()
            .create()
    }


    @Singleton
    @Provides
    fun providesAlbumDatabases(application: Application) : AlbumDatabase {
        return Room.databaseBuilder(
            application,
            AlbumDatabase::class.java,
            "album.db"
        ).build()
    }

    @Singleton
    @Provides
    fun providerAlbumRepository(albumApi: AlbumApi, albumDatabase: AlbumDatabase) : Repository {
        return RepositoryImpl(db = albumDatabase, albumApi = albumApi)
    }

    @Singleton
    @Provides
    fun providesUserCase(repository: Repository) : GetAlbum{
        return GetAlbum(repository)
    }

}