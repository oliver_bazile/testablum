package com.example.testalbum.data.repository

import com.example.testalbum.domain.model.Album
import com.example.testalbum.domain.repository.Repository
import com.example.testalbum.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeRepository : Repository {

    private val albumList = listOf(
        Album(
            albumId = 1,
            id = 1,
            title = "accusamus beatae ad facilis cum similique qui sunt",
            url = "https://via.placeholder.com/600/92c952",
            thumbnailUrl = "https://via.placeholder.com/150/92c952"
        ),
        Album(
            albumId = 1,
            id = 2,
            title = "reprehenderit est deserunt velit ipsam",
            url = "https=//via.placeholder.com/600/771796",
            thumbnailUrl = "https://via.placeholder.com/150/771796"
        )

    )

    override suspend fun getAlum(): Flow<Resource<List<Album>>> {
        return flow {
            emit(Resource.Loading(true))
            emit(Resource.Success(albumList))
        }
    }

}