package com.example.testalbum.presentation.fragment.album

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.testalbum.data.repository.FakeRepository
import com.example.testalbum.data.util.CoroutineRule
import com.example.testalbum.domain.buisness.GetAlbum
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AlbumViewModelTest {


    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    private lateinit var viewModel: AlbumViewModel

    private lateinit var fakeRepository: FakeRepository
    private lateinit var getAlbum: GetAlbum

    @get:Rule
    var coroutineRule = CoroutineRule()


    @Before
    fun setUp() {
        fakeRepository = FakeRepository()
        getAlbum = GetAlbum(fakeRepository)
        viewModel = AlbumViewModel(getAlbum)
    }

    @Test
    fun `test if it loads all the data`() = runBlocking {
        viewModel.getAllAlbum()
        assertEquals(2, viewModel.event.value.albumList.size)
    }

    @Test
    fun `test if the loading is false`() = runBlocking {
        viewModel.getAllAlbum()
        assertEquals(false, viewModel.event.value.isLoading)
    }
}